const presentation = document.querySelector('.card.presentation');
const description = document.querySelector('.card.description');

const showProfileButton = document.getElementById('showProfile');

presentation.classList.add('isVisible');
description.classList.add('isNotVisible');

showProfileButton.onclick = showProfileFuntion;

function showProfileFuntion(ev) {
    description.classList.add('isVisible');
    presentation.classList.add('isNotVisible');

    presentation.classList.remove('isVisible');
    description.classList.remove('isNotVisible');

    setTimeout(function () {
        presentation.classList.add('isVisible');
        description.classList.add('isNotVisible');
        description.classList.remove('isVisible');
        presentation.classList.remove('isNotVisible');
    }, 8000);
}
