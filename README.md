# Acerca de mí
¡Hola! 

Soy Andree, estoy tomando la ruta de aprendizaje 'HTML y CSS a Profundidad', en [platzi.com](https://platzi.com/),
actualmente soy Desarrollador Front End, y me agrada seguir aprendiento.

Trabajemos juntos, te dejo por acá mi contacto.
```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

En el presente proyecto, se maqueta un card de presentación, puede ser visualizado en distintos navegadores.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://card-lacm.netlify.app/)

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2022 LYAM ANDREE CRUZ MALERVA

